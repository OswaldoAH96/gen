from email.policy import default
from django.db import models
from django.utils.translation import gettext_lazy as _

from device_status.models import DeviceStatus
from device_type.models import DeviceType
# Create your models here.


class Device(models.Model):
    device_name = models.CharField(
        _("Device name"),
        max_length=50,
        unique=True,
    )
    discharge_date = models.DateTimeField(auto_now_add=True, )
    update_date = models.DateTimeField(auto_now=True, )
    current_power = models.IntegerField(_("Current power"), )
    device_status = models.ForeignKey(
        DeviceStatus,
        verbose_name=_("DeviceStatus"),
        on_delete=models.CASCADE,
        default=1,
    )
    device_type = models.ForeignKey(
        DeviceType,
        verbose_name=_("DeviceType"),
        on_delete=models.CASCADE,
        default=1,
    )

    class Meta:
        verbose_name = _("Device")
        verbose_name_plural = _("Devices")


    def __str__(self):
        return self.device_name
