# Generated by Django 4.0.3 on 2022-03-24 03:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('device_status', '0002_alter_devicestatus_status_name'),
        ('device', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='device',
            name='device_status',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='device_status.devicestatus', verbose_name=''),
        ),
    ]
