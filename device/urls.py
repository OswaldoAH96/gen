from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from device import views

urlpatterns = [
    path('devices/', views.DeviceList.as_view()),
    path('devices/<int:pk>', views.DeviceDetail.as_view()),
]