import django_filters
from device.models import Device

class DeviceFilter(django_filters.FilterSet):
    class Meta:
        model = Device
        fields = ['device_type']