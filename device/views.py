from device.models import Device
from device.filters import DeviceFilter
from device_status.models import DeviceStatus
from device_type.models import DeviceType
from device.serializers import DeviceSerializer
from reading.models import Reading
from rest_framework import generics, status
from rest_framework.response import Response


class DeviceList(generics.ListCreateAPIView):
    queryset = Device.objects.all()
    serializer_class = DeviceSerializer
    filterset_class = DeviceFilter

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            status_id = serializer.data['device_status']
            device_status = DeviceStatus.objects.get(pk=status_id)
            if device_status.status_name == "En operación":
                device = Device.objects.get(pk=int(serializer.data['id']))
                device_type = DeviceType.objects.get(
                    pk=serializer.data['device_type'])
                Reading(
                    device=device,
                    device_type=device_type,
                    current_power=device.current_power,
                ).save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DeviceDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Device.objects.all()
    serializer_class = DeviceSerializer

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.serializer_class(instance)
        data = dict(id=serializer.data['id'],
                    device_name=serializer.data['device_name'],
                    discharge_date=serializer.data['discharge_date'],
                    update_date=serializer.data['update_date'],
                    current_power=serializer.data['current_power'],
                    device_status=serializer.data['device_status'],
                    device_type=serializer.data['device_type'])
        if instance.device_status.status_name == 'En operación':
            Reading(
                device=instance,
                device_type=instance.device_type,
                current_power=instance.current_power,
            ).save()
        else:
            data['message'] = "No se puede realizar el registro dispositivo en mantenimiento"
        
        return Response(data, status=status.HTTP_200_OK)