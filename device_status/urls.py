from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from device_status import views

urlpatterns = [
    path('status/', views.DeviceList.as_view()),
    path('status/<int:pk>/', views.DeviceDetail.as_view()),
]