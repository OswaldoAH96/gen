from rest_framework import serializers
from device_status.models import DeviceStatus

class DeviceStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = DeviceStatus
        fields = '__all__'