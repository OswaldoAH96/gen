from device_status.models import DeviceStatus
from device_status.serializers import DeviceStatusSerializer
from rest_framework import generics


class DeviceList(generics.ListCreateAPIView):
    queryset = DeviceStatus.objects.all()
    serializer_class = DeviceStatusSerializer


class DeviceDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = DeviceStatus.objects.all()
    serializer_class = DeviceStatusSerializer