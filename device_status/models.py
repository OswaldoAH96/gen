from django.db import models
from django.utils.translation import gettext_lazy as _

# Create your models here.
class DeviceStatus(models.Model):
    status_name = models.CharField(_("Status name"), max_length=50,default='')

    class Meta:
        verbose_name = _("DeviceStatus")
        verbose_name_plural = _("DeviceStatuss")

    def __str__(self):
        return self.status_name
