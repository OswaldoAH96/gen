from django.db import models
from django.utils.translation import gettext_lazy as _

# Create your models here.
class DeviceType(models.Model):
    type_name = models.CharField(_("Type name"), max_length=50, default='')

    class Meta:
        verbose_name = _("DeviceType")
        verbose_name_plural = _("DeviceTypes")

    def __str__(self):
        return self.type_name
