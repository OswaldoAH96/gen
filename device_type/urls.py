from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from device_type import views

urlpatterns = [
    path('type/', views.DeviceList.as_view()),
    path('type/<int:pk>/', views.DeviceDetail.as_view()),
]