from device_type.models import DeviceType
from device_type.serializers import DeviceTypeSerializer
from rest_framework import generics


class DeviceList(generics.ListCreateAPIView):
    queryset = DeviceType.objects.all()
    serializer_class = DeviceTypeSerializer


class DeviceDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = DeviceType.objects.all()
    serializer_class = DeviceTypeSerializer