from django.apps import AppConfig


class DeviceTypeConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'device_type'
