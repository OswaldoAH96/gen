from django.http import Http404
from device.models import Device
from reading import serializers
from reading.models import Reading
from reading.serializers import ReadingSerializer
from reading.filters import ReadingFilter
from rest_framework.decorators import api_view
from rest_framework import generics, status
from rest_framework.response import Response
from django.db.models import Sum


class ReadingList(generics.ListAPIView):
    queryset = Reading.objects.all()
    serializer_class = ReadingSerializer
    filterset_class = ReadingFilter


class ReadingDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Reading.objects.all()
    serializer_class = ReadingSerializer


@api_view(['GET'])
def total_detail(request, pk):
    try:
        device = Device.objects.get(pk=pk)
        reading = Reading.objects.filter(
            device=device).values('device').annotate(
                energia=Sum('current_power'))
    except Device.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if request.method == 'GET':
        list_result = [entry for entry in reading]
        return Response(list_result[0])
    raise Http404