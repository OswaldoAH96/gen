from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from reading import views

urlpatterns = [
    path('reading/', views.ReadingList.as_view()),
    path('reading/<int:pk>/', views.ReadingDetail.as_view()),
    path('reading/<int:pk>/total', views.total_detail),
]