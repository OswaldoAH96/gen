import django_filters
from reading.models import Reading

class ReadingFilter(django_filters.FilterSet):
    class Meta:
        model = Reading
        fields = ['device', 'device_type']