from django.db import models
from django.utils.translation import gettext_lazy as _
from device.models import Device
from device_type.models import DeviceType


# Create your models here.
class Reading(models.Model):
    device = models.ForeignKey(
        Device,
        verbose_name=_("Tipo de dispositivo"),
        on_delete=models.CASCADE,
        default=1,
    )
    device_type = models.ForeignKey(
        DeviceType,
        verbose_name=_("Tipo de dispositivo"),
        on_delete=models.CASCADE,
        default=1,
    )
    current_power = models.IntegerField(_("Current power"), )
    created_at = models.DateTimeField(auto_now_add=True, )

    class Meta:
        verbose_name = _("Reading")
        verbose_name_plural = _("Readings")

    def __str__(self):
        return self.device.device_name
