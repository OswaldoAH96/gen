# EXAMEN PYTHON

El proyecto fue trabajado con:

```
Python 3.8
Django 3.2
djangorestframework 3.12
django-filter 21.1
```

## Pasos para levantar el proyecto

Para levantar el proyecto es necesario primero tener instalado `virtualenv (pip install virtualenv)` 

Se crea un entorno virtual con el comando `virtualenv .env`

En este caso se procedió a trabajar con la base de datos SQLite3 por lo que no es necesario configurar nada en base de datos

Para instalar las dependecias se tiene que hacer un `pip install -r requirements`

Seguidamente podemos ejecutar el siguiente comando `python manage.py runserver`,  debido a que ya va la base de datos con datos previamente inicializados, no es necesario ejecutar los comandos:
```
python manage.py makemigrations
python manage.py migrate
```

### URL del MicroServicio
Debido a problemas que tuve instalando swagger decidí deja las pantallas que crea django para poder tener una representación gráfica del lo que realiza el API

CRUD Dispositivos
- *GET Y POST*
    - https://oswaldo.pythonanywhere.com/devices/ (En la parte superior tiene el campo de filtros derecha)
- *GET PUT y DELETE*
    - https://oswaldo.pythonanywhere.com/devices/1

*Tabla de registro*
- *GET Y POST*
    - https://oswaldo.pythonanywhere.com/reading/ (En la parte superior tiene el campo de filtros derecha)
- *GET PUT y DELETE*
    - https://oswaldo.pythonanywhere.com/reading/1/total

